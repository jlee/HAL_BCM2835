;
; Copyright (c) 2012, RISC OS Open Ltd
; All rights reserved.
;
; Redistribution and use in source and binary forms, with or without
; modification, are permitted provided that the following conditions are met:
;     * Redistributions of source code must retain the above copyright
;       notice, this list of conditions and the following disclaimer.
;     * Redistributions in binary form must reproduce the above copyright
;       notice, this list of conditions and the following disclaimer in the
;       documentation and/or other materials provided with the distribution.
;     * Neither the name of RISC OS Open Ltd nor the names of its contributors
;       may be used to endorse or promote products derived from this software
;       without specific prior written permission.
;
; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
; AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
; ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
; LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
; CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
; SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
; INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
; CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
; POSSIBILITY OF SUCH DAMAGE.
;

; Interrupt handling - BCM2835, 2836, 2837

        EXPORT  Interrupt_Init

        EXPORT  ARM11_HAL_IRQEnable
        EXPORT  ARM11_HAL_IRQDisable
        EXPORT  ARM11_HAL_IRQClear
        EXPORT  ARM11_HAL_IRQSource
        EXPORT  ARM11_HAL_IRQStatus
        EXPORT  ARM11_HAL_FIQEnable
        EXPORT  ARM11_HAL_FIQDisable
        EXPORT  ARM11_HAL_FIQDisableAll
        EXPORT  ARM11_HAL_FIQClear
        EXPORT  ARM11_HAL_FIQSource
        EXPORT  ARM11_HAL_FIQStatus
        EXPORT  ARM11_HAL_IRQMax
        EXPORT  ARM11_HAL_IRQProperties

        EXPORT  QA7_HAL_IRQEnable
        EXPORT  QA7_HAL_IRQDisable
        EXPORT  QA7_HAL_IRQClear
        EXPORT  QA7_HAL_IRQSource
        EXPORT  QA7_HAL_IRQStatus
        EXPORT  QA7_HAL_FIQEnable
        EXPORT  QA7_HAL_FIQDisable
        EXPORT  QA7_HAL_FIQDisableAll
        EXPORT  QA7_HAL_FIQClear
        EXPORT  QA7_HAL_FIQSource
        EXPORT  QA7_HAL_FIQStatus
        EXPORT  QA7_HAL_IRQMax
        EXPORT  QA7_HAL_IRQProperties
        EXPORT  QA7_HAL_IRQSetCores
        EXPORT  QA7_HAL_IRQGetCores

        GET     Hdr:ListOpts
        GET     Hdr:Macros
        GET     Hdr:System

        GET     hdr.BCM2835
        GET     hdr.StaticWS


        AREA    |ARM$$code|, CODE, READONLY, PIC

; One-time initialisation

Interrupt_Init
        LDR     a1, PeriBase
        ADD     a1, a1, #IRQ_Base
        STR     a1, IRQ_Base_Address
        MOV     pc, lr


QA7_HAL_IRQEnable ROUT
        CMP     a1, #iDev_QA7_Max
        BHS     ExitZero
        SUBS    a2, a1, #iDev_QA7_Base
        BLT     HAL_IRQEnable_GPU
        MOV     a4, #1                ; Signal this is an IRQ enable event
ToggleIRQ
        AND     a3, a2, #3            ; Extract core number
        MOV     a2, a2, LSR #2        ; Bit/device number
        LDR     a1, IntBase
        AcquireSpinlock
        ADD     pc, pc, a2, LSL #2
        NOP
        B       ToggleIRQ_CNTPSIRQ
        B       ToggleIRQ_CNTPNSIRQ
        B       ToggleIRQ_CNTPHPIRQ
        B       ToggleIRQ_CNTPVIRQ
        B       ToggleIRQ_MBox0
        B       ToggleIRQ_MBox1
        B       ToggleIRQ_MBox2
        B       ToggleIRQ_MBox3
        B       ToggleIRQ_GPU
        B       ToggleIRQ_PMU
        B       ToggleIRQ_AXI
        B       ToggleIRQ_LocalTimer

ToggleIRQ_MBox0
ToggleIRQ_MBox1
ToggleIRQ_MBox2
ToggleIRQ_MBox3
        ADD     a1, a1, #QA7_CORE0_MBOX_INT_CTRL-QA7_CORE0_TIMER_INT_CTRL
ToggleIRQ_CNTPSIRQ
ToggleIRQ_CNTPNSIRQ
ToggleIRQ_CNTPHPIRQ
ToggleIRQ_CNTPVIRQ ROUT
        ; Work out bit number
        AND     a2, a2, #3
        TST     a4, #2
        MOVEQ   ip, #1
        MOVNE   ip, #16
        MOV     ip, ip, LSL a2
        ; Work out the register number
        ADD     a1, a1, a3, LSL #2
        ; Grab the state
        LDR     a2, [a1, #QA7_CORE0_TIMER_INT_CTRL]
        ; Calc new state
        ; Note that this is a trivial implementation which doesn't attempt to do anything special to deal with the case that having FIQ enabled will mask the IRQ
        TST     a4, #1
        BICEQ   a4, a2, ip
        ORRNE   a4, a2, ip
        STR     a4, [a1, #QA7_CORE0_TIMER_INT_CTRL]
        ; Calc old state, ignoring that FIQs will mask IRQs
        AND     a1, a2, ip
        ReleaseSpinlock
        MOV     pc, lr

ToggleIRQ_GPU ROUT
        ; Get old status
        LDR     a2, [a1, #QA7_GPU_INT_ROUTING]
        ; Calc new status (can only claim, not release)
        TST     a4, #1
        BEQ     %FT30
        TST     a4, #2
        MOV     ip, a2
        BFIEQ   ip, a3, #0, #2
        BFINE   ip, a3, #2, #2
        STR     ip, [a1, #QA7_GPU_INT_ROUTING]
30
        ReleaseSpinlock
        ; Calc old state
        TST     a4, #2
        MOVNE   a2, a2, LSR #2
        AND     a2, a2, #3
        CMP     a2, a3
        MOVEQ   a1, #1
        MOVNE   a1, #0
        MOV     pc, lr

ToggleIRQ_PMU ROUT
        ; Grab the old state before we overwrite it
        LDR     a2, [a1, #QA7_PMU_INT_ROUT_CLR]
        ; Calculate the bit to toggle
        ; Note that this is a trivial implementation which doesn't attempt to do anything special to deal with the case that having FIQ enabled will mask the IRQ
        TST     a4, #2
        MOVEQ   ip, #1
        MOVNE   ip, #16
        MOV     ip, ip, LSL a3
        ; Calculate the register to write to
        TST     a4, #1 ; EQ: Disable, NE: Enable
        ADDEQ   a1, a1, #QA7_PMU_INT_ROUT_CLR
        ADDNE   a1, a1, #QA7_PMU_INT_ROUT_SET
        STR     ip, [a1]
        ; Calc old state, ignoring that FIQs will mask IRQs
        AND     a1, a2, ip
        ReleaseSpinlock
        MOV     pc, lr

ToggleIRQ_AXI ROUT
        ; Only supported for core 0 IRQ
        TEQ     a3, #0
        TSTEQ   a4, #2
        BNE     ReleaseExitZero
        LDR     a2, [a1, #QA7_AXI_OUTSTANDING_IRQ]
        MOV     a3, a2
        BFI     a3, a4, #20, #1
        STR     a3, [a1, #QA7_AXI_OUTSTANDING_IRQ]
        AND     a1, a2, #1<<20
        ReleaseSpinlock
        MOV     pc, lr

ToggleIRQ_LocalTimer ROUT
        ; Calculate required routing value
        AND     ip, a4, #2
        ORR     a3, a3, ip, LSL #1
        ; Get old status
        LDR     a2, [a1, #QA7_LOCAL_INT_ROUTING]
        ; Can only claim, not release
        TST     a4, #1
        BEQ     %FT40
        MOV     a4, a2
        BFI     a4, a3, #0, #3
        STR     a4, [a1, #QA7_LOCAL_INT_ROUTING]
40
        ReleaseSpinlock
        ; Calc old state
        AND     a4, a4, #7
        CMP     a4, a3
        MOVEQ   a1, #1
        MOVNE   a1, #0

ARM11_HAL_IRQEnable ROUT
        CMP     a1, #iDev_ARM11_Max
        BHS     ExitZero
HAL_IRQEnable_GPU
        DoMemBarrier ip
        LDR     ip, IRQ_Base_Address
        ADD     ip, ip, #IRQ_EN1
        MOV     a2, #1
        AND     a3, a1, #&1F         ; get bit in register
        MOV     a2, a2, LSL a3       ; bitmask
        MOV     a3, a1, LSR #5       ; shift to get relevant register
        LDR     a4, [ip, a3, LSL #2] ; get old enable mask
        STR     a2, [ip, a3, LSL #2] ; enable our bit
        AND     a1, a2, a4           ; test our bit in old mask
        DoMemBarrier ip
        MOV     pc, lr

ReleaseExitZero
        ReleaseSpinlock
ExitZero
        MOV     a1, #0
        MOV     pc, lr

QA7_HAL_IRQDisable ROUT
        CMP     a1, #iDev_QA7_Max
        BHS     ExitZero
        SUBS    a2, a1, #iDev_QA7_Base
        BLT     HAL_IRQDisable_GPU
        MOV     a4, #0                ; Signal this is an IRQ disable event
        B       ToggleIRQ

ARM11_HAL_IRQDisable
        CMP     a1, #iDev_ARM11_Max
        BHS     ExitZero
HAL_IRQDisable_GPU
        DoMemBarrier ip
        LDR     ip, IRQ_Base_Address
        ADD     ip, ip, #IRQ_DIS1
        MOV     a2, #1
        AND     a3, a1, #&1F         ; get bit in register
        MOV     a2, a2, LSL a3       ; bitmask
        MOV     a3, a1, LSR #5       ; shift to get relevant register
        LDR     a4, [ip, a3, LSL #2] ; get old enable mask
        STR     a2, [ip, a3, LSL #2] ; disable our bit
        AND     a1, a2, a4           ; test our bit in old mask
        DoMemBarrier ip
        MOV     pc, lr

ARM11_HAL_IRQClear
ARM11_HAL_FIQClear
QA7_HAL_IRQClear
QA7_HAL_FIQClear
        ; There is no latching of interrupts in this interrupt controller,
        ; so nothing to clear
        MOV     pc, lr

QA7_GPU_IRQ_bit * 1<<((iDev_QA7_GPU-iDev_QA7_Base)>>2)

QA7_HAL_IRQSource
        DoMemBarrier ip
        LDR     a2, IntBase
        MRC     p15, 0, a1, c0, c0, 5 ; Read MPIDR
        AND     a3, a1, #3            ; Extract core number
        ASSERT  QA7_CORE1_IRQ_SOURCE-QA7_CORE0_IRQ_SOURCE = 4
        ADD     a2, a2, a3, LSL #2
        LDR     a2, [a2, #QA7_CORE0_IRQ_SOURCE]
        ADD     a3, a3, #iDev_QA7_Base
        ; Prioritise core-specific interrupts over general ones
        BIC     a1, a2, #QA7_GPU_IRQ_bit
        CLZ     a1, a1
        RSBS    a1, a1, #31
        ADDPL   a1, a3, a1, LSL #2
        BPL     %FT90
        TST     a2, #QA7_GPU_IRQ_bit
        BEQ     %FT90 ; No GPU interrupt, so must be spurious? (a1 already -1)
        ; Fall through to read GPU interrupt source
ARM11_HAL_IRQSource
        DoMemBarrier ip
        LDR     a2, IRQ_Base_Address
        LDRB    a1, [a2, #IRQ_PENDB]  ; note, LDRB so we ignore bits 8-31
        CLZ     a1, a1
        RSBS    a1, a1, #31
        ADDPL   a1, a1, #iDev_ARM_Timer ; 64
        BPL     %FT90
        LDR     a1, [a2, #IRQ_PEND2]
        CLZ     a1, a1
        RSBS    a1, a1, #31
        ADDPL   a1, a1, #iDev_GPU_HostPort ; 32
        BPL     %FT90
        LDR     a1, [a2, #IRQ_PEND1]
        CLZ     a1, a1
        RSB     a1, a1, #31
90
        DoMemBarrier ip
        MOV     pc, lr

QA7_HAL_IRQStatus ROUT
        CMP     a1, #iDev_QA7_Max
        BHS     ExitZero
        SUBS    a3, a1, #iDev_QA7_Base
        BLT     %FT50
        DoMemBarrier ip
        LDR     a2, IntBase
        AND     a1, a3, #3            ; Extract core number
        MOV     a3, a3, LSR #2        ; Bit/device number
        ASSERT  QA7_CORE1_IRQ_SOURCE-QA7_CORE0_IRQ_SOURCE = 4
        ADD     a2, a2, a1, LSL #2
        LDR     a2, [a2, #QA7_CORE0_IRQ_SOURCE]
        MOV     a2, a2, LSR a3
        AND     a2, a2, #1
        DoMemBarrier ip
        MOV     pc, lr

ARM11_HAL_IRQStatus
        ; This interrupt controller does not allow us to see the status of the interrupt request
        ; lines prior to masking by the interrupt enable register, which is the defined result
        ; of this call. The closest we can do is checking whether the stated device is interrupting.
        CMP     a1, #iDev_ARM11_Max
        BHS     ExitZero
50
        DoMemBarrier ip
        LDR     ip, IRQ_Base_Address
        ASSERT  IRQ_PENDB = 0
        MOV     a2, #1
        AND     a3, a1, #&1F         ; get bit in register
        MOV     a2, a2, LSL a3       ; bitmask
        MOV     a3, a1, LSR #5       ; shift to get relevant register
        SUBS    a3, a3, #2           ; rotate register because pending regs are in a different order
        ADDMI   a3, a3, #3
        LDR     a4, [ip, a3, LSL #2] ; get pending mask
        AND     a1, a2, a4           ; test our bit
        DoMemBarrier ip
        MOV     pc, lr

FIQEnable       *       1<<7
FIQSourceMask   *       &7F

QA7_HAL_FIQEnable ROUT
        CMP     a1, #iDev_QA7_Max
        BHS     ExitZero
        SUBS    a2, a1, #iDev_QA7_Base
        BLT     HAL_FIQEnable_GPU
        MOV     a4, #3                ; Signal this is a FIQ enable event
        B       ToggleIRQ

ARM11_HAL_FIQEnable
        CMP     a1, #iDev_ARM11_Max
        BHS     ExitZero
HAL_FIQEnable_GPU
        DoMemBarrier ip
        LDR     ip, IRQ_Base_Address

        LDRB    a3, [ip, #IRQ_FIQCTL]   ; LDRB helpfully masks out bits 8-31 for us
        ORR     a4, a1, #FIQEnable
        TEQ     a3, a4                  ; Z set => FIQs already enabled
        STR     a4, [ip, #IRQ_FIQCTL]

        MOVEQ   a1, #1
        MOVNE   a1, #0
        DoMemBarrier ip
        MOV     pc, lr

QA7_HAL_FIQDisable ROUT
        CMP     a1, #iDev_QA7_Max
        BHS     ExitZero
        SUBS    a2, a1, #iDev_QA7_Base
        BLT     HAL_FIQDisable_GPU
        MOV     a4, #2                ; Signal this is a FIQ disable event
        B       ToggleIRQ

ARM11_HAL_FIQDisable
        CMP     a1, #iDev_ARM11_Max
        BHS     ExitZero
HAL_FIQDisable_GPU
        DoMemBarrier ip
        LDR     ip, IRQ_Base_Address

        LDRB    a3, [ip, #IRQ_FIQCTL] ; LDRB helpfully masks out bits 8-31 for us
        ORR     a4, a1, #FIQEnable
        TEQ     a3, a4                  ; Z set => FIQs already enabled
        MOV     a4, #0
        STR     a4, [ip, #IRQ_FIQCTL]

        MOVEQ   a1, #1
        MOVNE   a1, #0
        DoMemBarrier ip
        MOV     pc, lr

QA7_HAL_FIQDisableAll ROUT
        LDR     a1, IntBase
        MRC     p15, 0, a3, c0, c0, 5 ; Read MPIDR
        AND     a3, a3, #3            ; Extract core number
        ; Disable PMU FIQ for this core
        MOV     ip, #16
        MOV     ip, ip, LSR a3
        STR     ip, [a1, #QA7_PMU_INT_ROUT_CLR]
        AcquireSpinlock
        ; Disable timer FIQs for this core
        ADD     ip, a1, a3, LSL #2
        LDR     a4, [ip, #QA7_CORE0_TIMER_INT_CTRL]
        BIC     a4, a4, #&F0
        STR     a4, [ip, #QA7_CORE0_TIMER_INT_CTRL]
        ; Disable mailbox FIQs for this core
        LDR     a4, [ip, #QA7_CORE0_MBOX_INT_CTRL]
        BIC     a4, a4, #&F0
        STR     a4, [ip, #QA7_CORE0_MBOX_INT_CTRL]
        ; If we own the local timer, disable its interrupt bit
        LDR     a4, [a1, #QA7_LOCAL_INT_ROUTING]
        AND     a4, a4, #7
        EOR     a4, a4, a3
        CMP     a4, #4
        BNE     %FT10
        LDR     a4, [a1, #QA7_LOCAL_TIMER_CTRL_STAT]
        TST     a4, #1<<29
        BEQ     %FT10
        BIC     a4, a4, #1<<29
        STR     a4, [a1, #QA7_LOCAL_TIMER_CTRL_STAT]
10
        ; Determine if we own the GPU FIQs
        LDR     a1, [a1, #QA7_GPU_INT_ROUTING]
        ReleaseSpinlock
        UBFX    a1, a1, #2, #2
        CMP     a1, a3
        MOVNE   pc, lr                  ; We're not the owner, so leave them alone
        ; We are the GPU FIQ owner, so fall through to GPU FIQ disable code
        ; (n.b. race condition here if another core tries to claim the GPU FIQs)

ARM11_HAL_FIQDisableAll
        DoMemBarrier ip
        LDR     ip, IRQ_Base_Address
        LDR     a1, [ip, #IRQ_FIQCTL]
        DoMemBarrier a2
        TST     a1, #FIQEnable
        MOVEQ   pc, lr                  ; FIQs weren't enabled

        MOV     a4, #0
        STR     a4, [ip, #IRQ_FIQCTL]
        DoMemBarrier ip
        MOV     pc, lr

QA7_HAL_FIQSource
        DoMemBarrier ip
        LDR     a2, IntBase
        MRC     p15, 0, a1, c0, c0, 5 ; Read MPIDR
        AND     a3, a1, #3            ; Extract core number
        ASSERT  QA7_CORE1_FIQ_SOURCE-QA7_CORE0_FIQ_SOURCE = 4
        ADD     a2, a2, a3, LSL #2
        LDR     a2, [a2, #QA7_CORE0_FIQ_SOURCE]
        ADD     a3, a3, #iDev_QA7_Base
        ; Prioritise core-specific interrupts over general ones
        BIC     a1, a2, #QA7_GPU_IRQ_bit
        CLZ     a1, a1
        RSBS    a1, a1, #31
        ADDPL   a1, a3, a1, LSL #2
        BPL     %FT90
        TST     a2, #QA7_GPU_IRQ_bit
        BEQ     %FT90 ; No GPU interrupt, so must be spurious? (a1 already -1)
        ; Fall through to read GPU interrupt source
ARM11_HAL_FIQSource
        ; There can only be one, the configured FIQ device
        DoMemBarrier ip
        LDR     ip, IRQ_Base_Address
        LDR     a1, [ip, #IRQ_FIQCTL]
        AND     a1, a1, #FIQSourceMask
90
        DoMemBarrier ip
        MOV     pc, lr

QA7_HAL_FIQStatus ROUT
        CMP     a1, #iDev_QA7_Max
        BHS     ExitZero
        SUBS    a3, a1, #iDev_QA7_Base ; For GPU interrupts just skip straight to reading the pending registers (shouldn't really matter whether we own the GPU FIQ or not)
        BLT     %FT50
        DoMemBarrier ip
        LDR     a2, IntBase
        AND     a1, a3, #3            ; Extract core number
        MOV     a3, a3, LSR #2        ; Bit/device number
        ASSERT  QA7_CORE1_FIQ_SOURCE-QA7_CORE0_FIQ_SOURCE = 4
        ADD     a2, a2, a1, LSL #2
        LDR     a2, [a2, #QA7_CORE0_FIQ_SOURCE]
        MOV     a2, a2, LSR a3
        AND     a2, a2, #1
        DoMemBarrier ip
        MOV     pc, lr

ARM11_HAL_FIQStatus
        ; As per HAL_IRQStatus, we can't see the true status of the interrupt,
        ; only whether it's currently firing.
        CMP     a1, #iDev_ARM11_Max
        BHS     ExitZero
50
        DoMemBarrier ip
        LDR     ip, IRQ_Base_Address
        LDR     a2, [ip, #IRQ_FIQCTL]
        AND     a2, a2, #FIQSourceMask
        CMP     a1, a2
        MOVEQ   a1, #1
        MOVNE   a1, #0
        DoMemBarrier ip
        MOV     pc, lr

ARM11_HAL_IRQMax
        MOV     a1, #iDev_ARM11_Max
        MOV     pc, lr

QA7_HAL_IRQMax
        MOV     a1, #iDev_QA7_Max
        MOV     pc, lr

; In: a1 = device number
; Out: a1 = IRQ mask
;      a2 = FIQ mask
;           bits 0-29 of each register give cores that the interrupt can be
;           assigned to
;           bit 30 = private flag
;           bit 31 = interrupt can be routed to multiple cores at once
QA7_HAL_IRQProperties
        CMP     a1, #iDev_QA7_Max
        MOVHS   a1, #0
        MOVHS   a2, #0
        MOVHS   pc, lr
        SUBS    a2, a1, #iDev_ARM11_Max
        BLO     ARM11_HAL_IRQProperties
        ; For now, just pretend that all the QA7 interrupts are core-specific
        ; In reality some of them can be switched (e.g. local timer)
        AND     a2, a2, #3 ; Get core number
        MOV     a1, #1
        MOV     a1, a1, LSL a2
        MOV     a2, a1
        MOV     pc, lr

ARM11_HAL_IRQProperties
        ; ARM_MiscGPU1 and above can't be used for FIQ
        CMP     a1, #iDev_ARM_MiscGPU1
        MOVHS   a2, #0
        MOVLO   a2, #1
        CMP     a1, #iDev_ARM11_Max
        MOVHS   a1, #0
        MOVLO   a1, #1
        MOV     pc, lr

; In: a1 = device number
;     a2 = desired core mask
; Out: a1 = actual core mask
QA7_HAL_IRQSetCores
QA7_HAL_IRQGetCores ; read-only version of IRQSetCores
        CMP     a1, #iDev_QA7_Max
        BHS     ExitZero
        SUBS    a2, a1, #iDev_ARM11_Max
        MOV     a1, #1
        ANDHS   a2, a2, #3
        MOVHS   a1, a1, LSL a2
        MOV     pc, lr

        END
